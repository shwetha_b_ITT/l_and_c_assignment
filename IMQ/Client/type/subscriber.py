import sys
sys.path.append('../../')
from Entities.Constants.Constants import Constants
from Server.Services.RequestHandler.RequestHandler import RequestHandler
from SharedLibraries.SocketHandler.SocketHandler import SocketHandler
from Server.Services.TopicService.TopicService import TopicService
from DatabaseHandler.DatabaseHandler import DatabaseHandler

database_handler = DatabaseHandler()
topic_service = TopicService()
socket_handler = SocketHandler()
constants = Constants()


class Subscriber():

    def subscriber_main(self, socket):
        self.socket_handler = SocketHandler()
        self.socket_handler.socket_obj = socket
        print("Welcome Subscriber")
        client_name = input("Please Enter your name : ")
        self.socket_handler.send_message(
            client_name, constants.SUBSCRIBER_ROLE)
        response = self.socket_handler.get_response()
        response = self.socket_handler.get_response()
        choice = input(response['data'])
        if(choice == "1"):
            self.subscribe_to_topic()
            self.socket_handler.close_socket()
        else:
            self.pull_message()
            self.socket_handler.close_socket()

    def subscribe_to_topic(self):
        self.socket_handler.send_message(None, constants.SUBSCRIBE_REQUEST)
        response = self.socket_handler.get_response()
        print(response['data'])
        topic_name = input("Please enter topic name : ")
        self.socket_handler.send_message(
            topic_name, constants.SUBSCRIBE_REQUEST)
        response = self.socket_handler.get_response()
        print(response['data'])

    def pull_message(self):
        self.socket_handler.send_message(None, constants.PULL_REQUEST)
        response = self.socket_handler.get_response()
        print(response['data'])
        topic_name = input("Please enter topic name to pull messages : ")
        self.socket_handler.send_message(topic_name, constants.PULL_REQUEST)
        response = self.socket_handler.get_response()
        print(response['data'])
