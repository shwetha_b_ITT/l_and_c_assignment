import sys
sys.path.append('../../')
from Entities.Constants.Constants import Constants
from Server.Services.RequestHandler.RequestHandler import RequestHandler
from SharedLibraries.SocketHandler.SocketHandler import SocketHandler
from Server.Services.TopicService.TopicService import TopicService
from DatabaseHandler.DatabaseHandler import DatabaseHandler

database_handler = DatabaseHandler()
topic_service = TopicService()
socket_handler = SocketHandler()
constants = Constants()


class Publisher():

    def publisher_main(self, socket):
        self.socket_handler = SocketHandler()
        self.socket_handler.socket_obj = socket
        print("You're now the Publisher")
        client_name = input("Enter your name : ")
        self.socket_handler.send_message(client_name, constants.PUBLISHER_ROLE)
        response = self.socket_handler.get_response()
        # print(response['data'])
        response = self.socket_handler.get_response()
        choice = input(response['data'])
        if(choice == "1"):
            self.create_topic()
            self.socket_handler.close_socket()
        else:
            self.push_message()
            self.socket_handler.close_socket()

    def create_topic(self):
        topic_name = input("Please enter topic name : ")
        self.socket_handler.send_message(
            topic_name, constants.TOPIC_CREATE_REQUEST)
        response = self.socket_handler.get_response()
        print(response['data'])

    def push_message(self):
        self.socket_handler.send_message(None, constants.PUSH_REQUEST)
        response = self.socket_handler.get_response()
        print(response['data'])
        data = input(
            "Please enter topic name and push message seperated with a space: ")
        self.socket_handler.send_message(data, constants.PUSH_REQUEST)
        response = self.socket_handler.get_response()
        print(response['data'])
