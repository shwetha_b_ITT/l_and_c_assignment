import sys
sys.path.append('../../')
from SocketHandler.SocketHandler import SocketHandler
from Client.Client import Client
import unittest


class ClientTest(unittest.TestCase):

    def setUp(self):
        self.client = Client()
        self.socket_handler = SocketHandler()

    def test_client_program(self):
        self.client.client_program(self.socket_handler)


if __name__ == '__main__':

    unittest.main()
