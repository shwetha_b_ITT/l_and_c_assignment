import sys
sys.path.append('../../')
from Server.Services.Queue.Queue import Queue
from DatabaseHandler.DatabaseHandler import DatabaseHandler
from Entities.Constants.Constants import Constants

database_handler = DatabaseHandler()
constants = Constants()
queue_obj = Queue()

class PublisherService():

    def push_messages(subscribers, topic_name, message_id, message):
        # for pushing message to queue
        queue_obj.push_messages(subscribers, topic_name, message_id, message)

        # for pushing messages to db
        # if(subscribers):
        #     for subscriber in subscribers:
        #         subscriber_id = subscriber[0]
        #         database_handler.insert_into_topic_queue(topic_name, subscriber_id, message_id, constants.MESSAGE_STATUS_READY)
        # return True