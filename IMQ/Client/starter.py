import sys
sys.path.append('../')
from DatabaseHandler.DatabaseHandler import DatabaseHandler
from type.subscriber import Subscriber
from type.publisher import Publisher
from SharedLibraries.SocketHandler.SocketHandler import SocketHandler


class Client(SocketHandler):

    def __init__(self):
        super().__init__()
        self.create_socket()
        self.connect_socket()

    def read_input_from_client(self):
        choice = input(
            "Please select one of the following choices \n1.Publisher \n2.Subscriber\n")
        if (int(choice) == 1):
            self.publisher = Publisher()
            self.publisher.publisher_main(self.socket_obj)
        elif (int(choice) == 2):
            self.subscriber = Subscriber()
            self.subscriber.subscriber_main(self.socket_obj)
        else:
            print('invalid choice selected.. please try again!!!')

    def get_response_from_server(self):
        response = self.get_response()
        print('Response from Server: '+response['data'])
        self.read_choice()
        self.close_socket()


if __name__ == "__main__":
    client = Client()
    client.read_input_from_client()
