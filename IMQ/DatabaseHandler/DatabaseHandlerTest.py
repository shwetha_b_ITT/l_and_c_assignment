import unittest
import sys
sys.path.append('../')
from DatabaseHandler import DatabaseHandler

class DatabaseHandlerTest(unittest.TestCase):
    def setUp(self):
        self.database_handler = DatabaseHandler()

    def test_create_db(self):
        self.assertTrue((type(self.database_handler.create_database())))

    # def test_get_messages(self):
    #     topic_name = "hu"
    #     subscriber_id = 4
    #     self.assertTrue((type(self.database_handler.get_messages(topic_name, subscriber_id))))

    def test_save_messages(self):
        topic_id = 4
        message = "hi"
        publisher_id = 9
        self.assertTrue((type(self.database_handler.save_messages(topic_id, message, publisher_id))))

    def test_create_message_status_table(self):
        self.assertTrue((type(self.database_handler.create_message_status_table())))

    def test_get_user_id(self):
        client_name = "sk"
        self.assertTrue((type(self.database_handler.get_user_id("sk"))))

    def test_get_topic_id(self):
        topic_name = "hu"
        self.assertTrue((type(self.database_handler.get_topic_id("sk"))))

    def test_get_topics(self):
        self.assertTrue((type(self.database_handler.get_topics())))

    def test_change_database(self):
        self.assertTrue((type(self.database_handler.change_database("IMQ_DB"))))

    def test_create_clients_table(self):
        self.assertTrue((type(self.database_handler.create_clients_table())))

    def test_create_topics_table(self):
        self.assertTrue((type(self.database_handler.create_topics_table())))

    def test_create_messages_table(self):
        self.assertTrue((type(self.database_handler.create_messages_table())))

    def test_insert_message_status(self):
        self.assertTrue((type(self.database_handler.insert_message_status())))

    def test_insert_into_table(self):
        table_name = "haya"
        data = "hi"
        self.assertTrue((type(self.database_handler.insert_into_table(table_name, data))))

    def test_show_tables(self):
        table_name = "haya_queue"
        self.assertTrue((type(self.database_handler.show_tables(table_name))))

    # def test_get_topics_list(self):
    #     table_name = "sk"
    #     self.assertTrue((type(self.database_handler.get_topics_list(table_name))))

    def test_add_subscription(self):
        topic_id = 1
        client_id = 5
        self.assertTrue((type(self.database_handler.add_subscription(topic_id, client_id))))

    def test_get_subcribers(self):
        topic_id = "1"
        self.assertTrue((type(self.database_handler.get_subcribers(topic_id))))

    # def test_get_publishers(self):
    #     topic_id = 1
    #     self.assertTrue((type(self.database_handler.get_publishers(topic_id))))

if __name__ == '__main__':
    unittest.main()