import sys
sys.path.append('../')
from Entities.Constants.Constants import Constants
from Entities.Utilities.utilities import Utilities
import mysql.connector
from Entities.LogHandler.LogHandler import LogHandler
from ExceptionHandler.DatabaseException.DatabaseException import DatabaseException

log_handler = LogHandler()
constants = Constants()
logger = log_handler.get_logger('imq_logger')
utilities = Utilities()


class DatabaseHandler:

    database_name = "IMQ_DB"
    global client_db

    def connect(self):
        try:
            self.client_db = mysql.connector.connect(
                user=constants.db_username, password=constants.db_password,
                host=constants.db_host
            )
            logger.info("Connected to SQL server")
        # except DatabaseException:
        except DatabaseException:
            # logger.error(str(err))
            logger.error("Connection failed to SQL server")

    def setup_db(self):
        self.create_clients_table()
        self.create_topics_table()
        self.create_subscriber_table()
        self.create_messages_table()
        self.create_message_status_table()
        self.insert_message_status()

    def __init__(self):
        self.connect()
        self.create_database()
        self.change_database(self.database_name)

    def create_database(self):
        try:
            self.client_cursor = self.client_db.cursor()
            self.execute_query(
                "CREATE DATABASE IF NOT EXISTS " + self.database_name)
        # except DatabaseException:
        #     logger.error(str(err))
        except DatabaseException:
            logger.error("Failed created database" + self.database_name)

    def change_database(self, database_name):
        try:
            query = 'USE ' + database_name
            self.execute_query(query)
        # except DatabaseException:
        #     logger.error(str(err))
        except DatabaseException:
            logger.error("Failed to create clients table")

    def create_clients_table(self):
        try:
            query = "CREATE TABLE IF NOT EXISTS clients (id INT AUTO_INCREMENT PRIMARY KEY, client_name VARCHAR(255) NOT NULL UNIQUE, type ENUM('1','2'))"
            self.execute_query(query)
            # except DatabaseException:
            #     logger.error(str(err))
        except DatabaseException:
            logger.error("Failed to create clients table")

    def create_topics_table(self):
        try:
            query = 'CREATE TABLE IF NOT EXISTS topics (id INT AUTO_INCREMENT PRIMARY KEY, topic_name VARCHAR(255) NOT NULL UNIQUE,created_by INT, CONSTRAINT topics_fk_clients FOREIGN KEY (created_by) REFERENCES clients(id))'
            self.execute_query(query)
        # except DatabaseException:
        #     logger.error(str(err))
        except DatabaseException:
            logger.error("Failed to create topics table")

    def create_messages_table(self):
        try:
            query = 'CREATE TABLE IF NOT EXISTS `messages` (`id` INT AUTO_INCREMENT PRIMARY KEY, `created_at` datetime DEFAULT NOW(), message VARCHAR(255),created_by INT NOT NULL,topic_id INT NOT NULL,expires_at datetime, CONSTRAINT messages_fk_topics FOREIGN KEY (topic_id) REFERENCES topics(id),CONSTRAINT messages_fk_clients FOREIGN KEY (created_by) REFERENCES clients(id))'
            self.execute_query(query)
        # except DatabaseException:
        #     logger.error(str(err))
        except DatabaseException:
            logger.error("Failed to create messages table")

    def insert_message_status(self):
        try:
            query = 'INSERT IGNORE INTO `message_status`(`status`) VALUES("READY"),("DELIVERED"),("DEAD")'
            self.execute_query(query)
        # except DatabaseException:
        #     logger.error(str(err))
        except DatabaseException:
            logger.error("Failed to create messages table")

    def insert_into_table(self, table_name, data):
        try:
            query = "INSERT into `"+table_name + \
                "`(`time_stamp`,`info`) values( NOW(),'"+data+"')"
            self.execute_query(query)
            logger.info("Inserted value into table " + table_name)
        # except DatabaseException:
        #     logger.error(str(err))
        except DatabaseException:
            logger.error("Failed to insert value into table" + table_name)

    def insert_into_topic_table(self, table_name, data):
        try:
            query = "INSERT into `"+table_name + \
                "`(`time_stamp`,`topics_list`) values( NOW(),'"+data+"')"
            self.execute_query(query)
            logger.info("Inserted value into table " + table_name)
        # except DatabaseException:
        #     logger.error(str(err))
        except DatabaseException:
            logger.error("Failed to insert value into table" + table_name)

    def create_reference_table(self):
        try:
            create_reference_table_query = 'CREATE TABLE IF NOT EXISTS reference (id INT AUTO_INCREMENT PRIMARY KEY, client_name VARCHAR(255) NOT NULL UNIQUE)'
            self.execute_query(create_reference_table_query)
            logger.info("Created reference table")
        # except DatabaseException:
        #     logger.error(str(err))
        except DatabaseException:
            logger.error("Failed to create reference table")

    def insert_into_reference_table(self, client_name):
        try:
            insert_into_query = "INSERT IGNORE INTO `reference` (`client_name`) values('" + \
                client_name+"')"

            self.execute_query(insert_into_query)

            logger.info("Inserted value into reference table" + client_name)
        except DatabaseException:
            logger.error(str(err))
            logger.error("Failed to insert values into reference table")

    def delete_table(self, table_name):
        try:
            delete_query = 'DROP TABLE IF EXISTS `"+table_name+"`'

            self.execute_query(delete_query)

        except DatabaseException:
            logger.error(str(err))
            logger.error("Failed to delete table " + table_name)

    def delete_from_table(self, table_name, client_name):
        try:
            delete_from_query = 'DELETE FROM `"+table_name+"` WHERE topics_list = `"+client_name+"`'

            self.execute_query(delete_from_query)

        except DatabaseException:
            logger.error(str(err))
            logger.error("Failed to delete table " + table_name)

    def show_tables(self, table_name):
        try:
            show_table_query = "SELECT * from `"+table_name+"`"

            self.execute_query(show_table_query)

        except DatabaseException:
            logger.error(str(err))

    def get_topics_list(self, table_name):
        try:
            sql_select_Query = "SELECT server_response from `"+table_name+"`"
            records = self.fetch_query(sql_select_Query)

            if records:
                return records
        except DatabaseException:
            logger.error(str(err))

    def get_messages(self, table_name):
        try:
            sql_select_Query = "SELECT * from `"+table_name+"`"
            self.execute_query(sql_select_Query)
            messages = cursor.fetchall()
            return messages
        except DatabaseException:
            logger.error(str(err))

    def database_main(self, table_name, data):
        self.connect()
        self.create_database()
        self.change_database(self.database_name)
        self.create_reference_table()
        self.insert_into_reference_table(table_name)
        self.create_table(table_name)
        self.insert_into_table(table_name, data)

    def fetch_query(self, query):
        try:
            self.client_cursor.execute(query)
            result = self.client_cursor.fetchall()
            return result
        except DatabaseException:
            exit()

    def get_subscribed_topics(self, subscriber_id):
        try:
            print(subscriber_id)
            query = "SELECT id,topic_name from `topics` WHERE id IN(SELECT topic_id from subscribers WHERE `client_id` = '"+str(
                subscriber_id)+"')"
            print(query)
            topics = self.fetch_query(query)
            if topics:
                return topics
            return False
        except DatabaseException:
            exit()

    def fetchone_query(self, query):
        try:

            self.client_cursor.execute(query)

            result = self.client_cursor.fetchone()
            if result:
                return result[0]
            return False
        except DatabaseException:
            exit()

    def add_subscription(self, topic_id, client_id):
        try:
            query = "INSERT into `subscribers` (`topic_id`,`client_id`) values('"+str(
                topic_id)+"','"+str(client_id)+"')"
            self.execute_query(query)
        except DatabaseException:
            exit()

    def get_subcribers(self, topic_id):
        try:
            query = 'SELECT client_id from subscribers WHERE topic_id = '+topic_id
            subscribers_list = self.fetch_query(query)
            print(self.fetch_query(query))
            print("database"+str(subscribers_list))
            if subscribers_list:
                return subscribers_list
            return False
        except DatabaseException:
            exit()

    def get_publishers(self, topic_id):
        try:
            query = 'SELECT server_response from publishers'
            publishers_list = self.fetch_query(query)
            if publishers_list:
                return publishers_list
            return False
        except DatabaseException:
            exit()

    def save_messages(self, topic_id, message, publisher_id):
        try:

            query = "INSERT into `messages` (`message`,`created_by`,`topic_id`) values('" + \
                message+"','"+publisher_id+"','"+topic_id+"')"
            self.execute_query(query)

            return self.client_cursor._insert_id
        except DatabaseException:
            exit()

    def get_messages(self, topic_name, subscriber_id):
        try:
            query = 'SELECT `message` FROM `messages` WHERE `id` IN (SELECT `message_id` from `'+str(
                topic_name)+'_queue` WHERE subscriber_id = '+str(subscriber_id)+' AND message_status_id = 1)'
            messages_list = self.fetch_query(query)

            if messages_list:
                return messages_list
            return False
        except DatabaseException:
            exit()

    def update_message_status(self, topic_name, subscriber_id):
        try:
            query = 'UPDATE `'+str(topic_name)+'_queue` SET message_status_id = (SELECT id from message_status WHERE status = "DELIVERED") WHERE subscriber_id = '+str(
                subscriber_id)+' AND message_status_id = (SELECT id from message_status WHERE status = "READY")'
            messages_list = self.execute_query(query)

            if messages_list:
                return messages_list
            return False
        except DatabaseException:
            exit()

    def get_topics(self):
        try:
            query = "SELECT id,topic_name FROM `topics`"
            topics = self.fetch_query(query)
            if topics:
                return topics
            return False
        except DatabaseException:
            exit()

    def create_subscriber_table(self):
        try:
            query = "CREATE TABLE IF NOT EXISTS subscribers (id INT AUTO_INCREMENT PRIMARY KEY, client_id  INT NOT NULL, topic_id INT NOT NULL, CONSTRAINT fk_topics FOREIGN KEY (topic_id) REFERENCES topics(id),CONSTRAINT subscribers_fk_clients FOREIGN KEY (client_id) REFERENCES clients(id))"
            self.execute_query(query)
        except DatabaseException:
            exit()

    def create_message_status_table(self):
        try:
            query = 'CREATE TABLE IF NOT EXISTS `message_status`(`id` INT AUTO_INCREMENT PRIMARY KEY,`status` VARCHAR(255) NOT NULL UNIQUE)'
            self.execute_query(query)
        except DatabaseException:
            exit()

    def fetch_query(self, query):
        try:
            self.client_cursor.execute(query)
            result = self.client_cursor.fetchall()
            print(result)
            if result:
                return result
            return False
        except DatabaseException:
            exit()

    def insert_into_clients_table(self, client_name, type):
        try:
            query = "INSERT into `clients` (`client_name`,`type`) values('" + \
                client_name+"','"+str(type)+"')"
            self.execute_query(query)

        except DatabaseException:
            exit()

    def insert_into_topic_table(self, topic_name, message, client_name):
        try:
            publisher_id = str(self.get_user_id(client_name))
            query = "INSERT into `"+topic_name + \
                "`(`received_time`,`data`,`created_by`) values( NOW(),'" + \
                message+"',"+str(publisher_id)+")"
            self.execute_query(query)

        except DatabaseException:
            exit()

    def get_user_id(self, client_name):
        try:
            query = "SELECT id from clients WHERE client_name = '"+client_name+"'"
            return self.fetchone_query(query)
        except DatabaseException:
            exit()

    def insert_into_topics_table(self, topic_name, client_name):
        try:

            publisher_id = str(self.get_user_id(client_name))
            query = "INSERT into `topics` (`topic_name`,`created_by`) values('" + \
                topic_name+"','"+str(publisher_id)+"')"
            self.execute_query(query)

        except DatabaseException:
            exit()

    def get_topic_id(self, topic_name):
        try:
            query = "SELECT id from topics WHERE topic_name = '"+topic_name+"'"
            return self.fetchone_query(query)
        except DatabaseException:
            exit()

    def get_message_id(self, message):
        try:
            query = "SELECT id from messages WHERE message like '"+message+"'"
            return self.fetchone_query(query)
        except DatabaseException:
            exit()

    def add_subscription(self, topic_id, client_id):
        try:
            query = "INSERT into `subscribers` (`topic_id`,`client_id`) values('"+str(
                topic_id)+"','"+str(client_id)+"')"
            self.execute_query(query)

        except DatabaseException:
            exit()

    def save_messages(self, topic_id, message, publisher_id):
        try:
            query = "INSERT into `messages` (`message`,`created_by`,`topic_id`) values('" + \
                message+"','"+str(publisher_id)+"','"+str(topic_id)+"')"
            self.execute_query(query)
            return self.get_message_id(message)
        except DatabaseException:
            exit()

    def create_queue_for_topic(self, topic_name):
        try:
            query = 'CREATE TABLE IF NOT EXISTS `'+topic_name+'_queue` (`id` INT AUTO_INCREMENT PRIMARY KEY, `message_id` INT,`subscriber_id` INT NOT NULL,message_status_id INT NOT NULL DEFAULT 1,CONSTRAINT '+topic_name + \
                '_queue_fk_clients FOREIGN KEY (subscriber_id) REFERENCES clients(id), CONSTRAINT '+topic_name + \
                '_queue_fk_status FOREIGN KEY (message_status_id) REFERENCES message_status(id),CONSTRAINT ' + \
                topic_name + \
                    '_queue_fk_message FOREIGN KEY (message_id) REFERENCES messages(id))'
            self.execute_query(query)
        except DatabaseException:
            exit()

    def insert_into_topic_queue(self, topic_name, subscriber_id, message_id, message_status_id):
        try:
            query = "INSERT into `"+topic_name + \
                "_queue` (`message_id`,`subscriber_id`, `message_status_id`) values('" + \
                str(message_id)+"','"+str(subscriber_id) + \
                "', '"+str(message_status_id)+"')"
            self.execute_query(query)
        except DatabaseException:
            exit()

    def execute_query(self, query):
        try:
            if(self.client_db.in_transaction):
                self.client_db.rollback()
            self.client_db.start_transaction()
            self.client_cursor.execute(query)
            self.client_db.commit()
        except DatabaseException:
            self.client_db.rollback()
