import os
import sys
sys.path.append('../../')
from Entities.Utilities.utilities import Utilities
from ExceptionHandler.FileException.FileException import FileException

utilities = Utilities()


class FileHandler:

    def create_directory(self, dir_name):
        try:
            parent_dir = os.getcwd()
            new_path = os.path.join(parent_dir, '../')
            path = os.path.join(new_path, dir_name)
            mode = 0o666

            if not os.path.exists(path):
                os.mkdir(path, mode)
            return path
        except FileException:
            logger.error("failed to create dir")

    def append_message(self, filename, data):
        try:
            with open(filename, "a") as file:
                file.write(str(utilities.get_time_stamp()) + ':' + data + '\n')
        except FileException:
            logger.error("failed to append")

    def write_message(self, filename, data):
        try:
            with open(filename, "w") as file:
                file.write(str(utilities.get_time_stamp()) + ':' + data + '\n')
        except FileException:
            logger.error("failed to write message")

    def write_into_file(self, filename, dir_path, data):
        try:
            if os.path.exists(os.path.dirname(dir_path)):
                filename = dir_path + '/' + filename
                if os.path.exists(filename):
                    self.append_message(filename, data)

                else:
                    self.write_message(filename, data)
            else:
                filename = dir_path + '/' + filename
                if os.path.exists(filename):
                    self.append_message(filename, data)

                else:
                    self.write_message(filename, data)
        except FileException:
            logger.error("failed to write into file")
