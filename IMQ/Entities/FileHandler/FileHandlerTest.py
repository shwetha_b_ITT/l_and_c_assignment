import sys
sys.path.append('../../')
import unittest
from FileHandler.FileHandler import FileHandler
import os

class FileHandlerTest(unittest.TestCase):
    def setUp(self):
        self.file_handler = FileHandler()

    def test_create_directory(self):
        test_path = os.getcwd() + "\\" + "testDirectory"
        self.assertEqual(test_path, self.file_handler.create_directory("testDirectory"))

    def test_append_message(self):
        self.assertTrue((type(self.file_handler.append_message("testFile.txt", "test1"))))

    def test_write_message(self):
        self.assertTrue((type(self.file_handler.write_message("testFile.txt", "testMessage1"))))

    def test_write_into_file(self):
        self.assertTrue((type(self.file_handler.write_into_file("testFile.txt", "testDirectory", "writingIntoFile"))))

if __name__ == '__main__':
    unittest.main()
