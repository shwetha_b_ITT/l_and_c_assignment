class Constants:
    database_name = "IMQ_DB"
    db_username = "root"
    db_password = "Skrishna@11"
    db_host = "localhost"

    PORT = 8000
    MAX_CONNECTION = 10
    DIRECTORY_NAME = "Output"

    NO_REQUEST = 0
    PUBLISHER_ROLE = 1
    SUBSCRIBER_ROLE = 2
    TOPIC_CREATE_REQUEST = 3
    PUSH_REQUEST = 4
    PULL_REQUEST = 5
    SUBSCRIBE_REQUEST = 6

    MESSAGE_STATUS_READY = 1
    MESSAGE_STATUS_DEAD = 3
    MESSAGE_STATUS_DELIVERED = 2
