import sys
sys.path.append('../')
from logging.handlers import TimedRotatingFileHandler  # when file is out of space, new file created on its own
import datetime
import logging
import os
import time

class LogHandler():

    FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")
    LOG_FILE = "C:/l_and_c_assignment/IMQ/Logs/messageSystem.log"

    def call_logger(self):
        logger = self.get_logger(self.LOG_FILE)
        return logger

    def get_logger(self, logger_name):
        logger = logging.getLogger(logger_name)
        logger.setLevel(logging.DEBUG) # better to have too much log than not enough
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(self.FORMATTER)
        file_handler = TimedRotatingFileHandler(self.LOG_FILE, when='midnight')
        file_handler.setFormatter(self.FORMATTER)

        if not logger.handlers:
            logger.addHandler(console_handler)
            logger.addHandler(file_handler)  # with this pattern, it's rarely necessary to propagate the error up to parent

        logger.propagate = False
        return logger