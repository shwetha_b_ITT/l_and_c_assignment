import sys
sys.path.append('../../')
from Server.Services.RequestHandler.RequestHandler import RequestHandler
from SharedLibraries.SocketHandler.SocketHandler import SocketHandler
from Entities.Constants.Constants import Constants
from Entities.LogHandler.LogHandler import LogHandler
from Entities.FileHandler.FileHandler import FileHandler
from DatabaseHandler.DatabaseHandler import DatabaseHandler

log_handler = LogHandler()
logger = log_handler.get_logger('imq_logger')
constants = Constants()


class ClientThreadHandler():

    def __init__(self, socket, address):
        self.socket_handler = SocketHandler()
        self.socket_handler.set_socket(socket)
        self.multi_threaded_client(socket, address)

    def multi_threaded_client(self, connection, address):
        while True:
            response_to_send = 'Welcome to the Server'
            self.socket_handler.send_message(
                response_to_send, constants.NO_REQUEST)
            data = self.socket_handler.get_response()
            if not data:
                break
            if(data['request_type'] == constants.PUBLISHER_ROLE):
                RequestHandler(self.socket_handler,
                               data['request_type'], data['data'])
            if(data['request_type'] == constants.SUBSCRIBER_ROLE):
                RequestHandler(self.socket_handler,
                               data['request_type'], data['data'])
        connection.close()
        exit()
