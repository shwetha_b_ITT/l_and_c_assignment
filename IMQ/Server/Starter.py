import sys
sys.path.append('../')
from DatabaseHandler.DatabaseHandler import DatabaseHandler
from ClientThreadHandler.ClientThreadHandler import ClientThreadHandler
import _thread
from Entities.LogHandler.LogHandler import LogHandler
from SharedLibraries.SocketHandler.SocketHandler import SocketHandler

log_handler = LogHandler()
logger = log_handler.get_logger('imq_logger')


class Server(SocketHandler):

    client_count = 0

    def __init__(self):
        super().__init__()

    def server_program(self):
        self.database_handler = DatabaseHandler()
        self.database_handler.setup_db()
        self.create_socket()
        self.bind_socket()
        self.accept_connections()

    def start_a_new_client(self, connection, address):
        client_thread_handler = ClientThreadHandler(connection, address)

    def accept_connections(self):
        while True:
            self.connection, self.address = self.accept_connection()
            if(self.connection):
                logger.info("Connection has been established " + "IP : " +
                            self.address[0] + " Port : " + str(self.address[1]) + " ")
                _thread.start_new_thread(
                    self.start_a_new_client, (self.connection, self.address))
        self.client_count += 1
        # self.socket_obj.close()


if __name__ == '__main__':
    server = Server()
    server.server_program()
