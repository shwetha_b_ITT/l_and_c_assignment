import unittest
import sys
sys.path.append('../')
from Starter import Server
from SocketHandler.SocketHandler import SocketHandler

class ServerTest(unittest.TestCase):
    def setUp(self):
        self.server = Server()
        self.socket_handler = SocketHandler()

    def test_server_program(self):
        self.server.server_program(self.socket_handler)

if __name__ == '__main__':
    unittest.main()