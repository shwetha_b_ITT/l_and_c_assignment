import sys
sys.path.append('../../../')
from Server.Services.Queue.Queue import Queue
from SharedLibraries.SocketHandler.SocketHandler import SocketHandler
from Entities.FileHandler.FileHandler import FileHandler
from Entities.Constants.Constants import Constants
from SharedLibraries.Parser.Parser import Parser
from DatabaseHandler.DatabaseHandler import DatabaseHandler
from Server.Services.MessageHandler.MessageHandler import MessageHandler
from Server.Services.TopicService.TopicService import TopicService

file_handler = FileHandler()
database_handler = DatabaseHandler()
message_handler = MessageHandler()
constants = Constants()
queue_obj = Queue()
topic_service = TopicService()


class RequestHandler():

    def __init__(self, socket_handler, role, client_name):
        self.socket_handler = socket_handler
        self.client_name = client_name
        self.handle_request(client_name, role)

    def handle_request(self, name, role):
        if(role):
            if(int(role) == 1):
                response_to_send = 'Authentication successful\n Please select one of the option 1.Create Topic 2.Push Message : '
            else:
                response_to_send = 'Authentication successful\n Please select one of the option 1.Subscribe to Topic 2.Pull Messages : '
            if not database_handler.get_user_id(name):
                database_handler.insert_into_clients_table(name, role)
            else:
                database_handler.get_user_id(name)
            self.socket_handler.send_message(
                response_to_send, constants.NO_REQUEST)
            response = self.socket_handler.get_response()
            if(response['request_type'] == constants.TOPIC_CREATE_REQUEST):
                self.handle_topic_creation(response['data'])
            if(response['request_type'] == constants.PUSH_REQUEST):
                self.push_message()
            if(response['request_type'] == constants.SUBSCRIBE_REQUEST):
                self.add_subscription(self.client_name)
            if(response['request_type'] == constants.PULL_REQUEST):
                self.handle_pull_request()
        else:
            response_to_send = 3
            self.socket_handler.send_message(
                response_to_send, constants.LOGIN_REQUEST)
        self.socket_handler.close_socket()
        exit()

    def push_message(self):
        self.socket_handler.send_message(
            self.get_topics_names(), constants.PUSH_REQUEST)
        message = self.socket_handler.get_response()['data']
        print(message)
        data = message.split(' ', 2)
        topic_name = data[0]
        message = data[1]
        topic_id = str(database_handler.get_topic_id(topic_name))
        print(topic_id)
        publisher_id = str(database_handler.get_user_id(self.client_name))
        message_id = database_handler.save_messages(
            topic_id, message, publisher_id)
        print(message_id)
        subscribers = database_handler.get_subcribers(topic_id)
        print(subscribers)
        if queue_obj.push_messages(subscribers, topic_name, message_id, message):
            print("pushed message to queue successfully")
        else:
            print("failed to push to queue")
        if(subscribers):
            for subscriber in subscribers:
                subscriber_id = subscriber[0]
                database_handler.insert_into_topic_queue(
                    topic_name, subscriber_id, message_id, constants.MESSAGE_STATUS_READY)
        response_to_send = 'Pushed message successfully\n'
        self.socket_handler.send_message(
            response_to_send, constants.PUSH_REQUEST)

    def handle_topic_creation(self, topic_name):
        if not database_handler.get_topic_id(topic_name):
            message_status_id = constants.MESSAGE_STATUS_READY
            database_handler.insert_into_topics_table(
                topic_name, self.client_name)
            topic_service.create_topic_queue(topic_name)
            response_to_send = 'Topic created successfully\n'
            self.socket_handler.send_message(
                response_to_send, constants.TOPIC_CREATE_REQUEST)
        else:
            response_to_send = 'Topic exists with that name\n'
            self.socket_handler.send_message(
                response_to_send, constants.TOPIC_CREATE_REQUEST)

    def add_subscription(self, client_name):
        self.socket_handler.send_message(
            self.get_topics_names(), constants.PUSH_REQUEST)
        topics_list = self.get_topics_names()
        topic_name = self.socket_handler.get_response()['data']
        topic_id = database_handler.get_topic_id(topic_name)
        subscriber_id = database_handler.get_user_id(client_name)
        database_handler.add_subscription(str(topic_id), str(subscriber_id))
        response_to_send = 'Subscribed successfully'
        self.socket_handler.send_message(
            response_to_send, constants.PUSH_REQUEST)

    def get_messages(self, topic_name):
        print("getting messages .. ")
        subscriber_id = str(database_handler.get_user_id(self.client_name))
        messages = message_handler.get_messages(topic_name, subscriber_id)
        message_handler.update_message_status(topic_name, subscriber_id)
        messages_to_send = ''
        if(messages):
            for message in messages:
                messages_to_send = messages_to_send + message[0] + '\n'
            return messages_to_send
        else:
            print("no messages for the topic\n" + topic_name)
            return False

    def get_topics_names(self):
        topics = topic_service.get_topics_names()
        messages_to_send = ''
        if(topics):
            for topic in topics:
                messages_to_send = messages_to_send + topic[1] + '\n'
            return messages_to_send
        else:
            print("no topics found\n")
            return False

    def get_subscribed_topics(self, subscriber_name):
        subscriber_id = database_handler.get_user_id(subscriber_name)
        print(subscriber_id)
        topics = topic_service.get_subscribed_topics(subscriber_id)
        print("subscriber_id : " + str(subscriber_id))
        messages_to_send = ''
        if(topics):
            for topic in topics:
                messages_to_send = messages_to_send + topic[1]+'\n'
            return messages_to_send
        return False

    def handle_pull_request(self):
        self.get_subscribed_topics(self.client_name)
        self.socket_handler.send_message(self.get_subscribed_topics(
            self.client_name), constants.PULL_REQUEST)
        topic_name = self.socket_handler.get_response()['data']
        subscriber_id = database_handler.get_user_id(self.client_name)
        response_to_send = message_handler.get_messages(topic_name, subscriber_id)
        if not (response_to_send):
            response_to_send = 'No Messages, pulled successfully'
        self.socket_handler.send_message(
            response_to_send, constants.PULL_REQUEST)