import sys
sys.path.append('../../../')
from SharedLibraries.SocketHandler.SocketHandler import SocketHandler
from DatabaseHandler.DatabaseHandler import DatabaseHandler
from Server.Services.Queue.Queue import Queue

database_handler = DatabaseHandler()
socket_handler = SocketHandler()
queue_obj = Queue()


class TopicService:

    def get_topics_names(self):
        topics = database_handler.get_topics()
        return topics

    def create_topic_queue(self, topic_name):
        #using queue
        queue_obj.create_queue_for_topic(topic_name)
        #using db
        # database_handler.create_queue_for_topic(topic_name)

    def get_subscribed_topics(self, subscriber_id):
        print(subscriber_id)
        subscribed_topics = database_handler.get_subscribed_topics(subscriber_id)
        return subscribed_topics
