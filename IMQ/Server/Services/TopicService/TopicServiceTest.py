import sys
sys.path.append('../../../')
import unittest
from Server.Services.TopicService.TopicService import TopicService

topic_service = TopicService()
class TopicServiceTest(unittest.TestCase):
    def test_get_topics_names(self):
        self.assertTrue(type(topic_service.get_topics_names()))

    def test_create_topic_queue(self):
        topic_name = "s1"
        self.assertTrue(type(topic_service.create_topic_queue(topic_name)))

    def test_get_subscribed_topics(self):
        subscriber_id = 4
        self.assertTrue(type(topic_service.get_subscribed_topics(subscriber_id)))

if __name__ == '__main__':
    unittest.main()