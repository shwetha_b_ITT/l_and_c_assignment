import sys
sys.path.append('../')
import datetime
from DatabaseHandler.DatabaseHandler import DatabaseHandler
from Entities.Constants.Constants import Constants

queues = {'dead_queue':[]}
constants = Constants()
database_handler = DatabaseHandler()

class Queue():

    def enqueue(self, topic_name, subscriber_id, message):
        NextDay_Date = datetime.datetime.today() + datetime.timedelta(days=1)
        expires_at = NextDay_Date.strftime ('%d%m%Y')
        message = {'message':message, 'subscriber_id':subscriber_id, 'message_status':constants.MESSAGE_STATUS_READY, 'expires_at':expires_at}
        queues[topic_name].insert(len(queues[topic_name]), message)

    def dequeue(self, topic_name, message):
        queues[topic_name].remove(message)

    def get_messages(self, topic_name, client_id):
        if(queues.__contains__(topic_name)):
            messages_in_channel = queues[topic_name]
            index = 0
            messages_to_send = ''
            current_date = datetime.datetime.today()
            if(len(messages_in_channel)):
                for message in messages_in_channel:
                    if(int(message['subscriber_id']) == int(client_id) and message['message_status'] == constants.MESSAGE_STATUS_READY):
                        messages_to_send = messages_to_send + "\n" + message['message']
                        queues[topic_name][index]['message_status'] = 2
                        expire_date = datetime.datetime.strptime(message['expires_at'], '%d%m%Y')
                        if(expire_date > current_date):
                            self.dequeue(topic_name,message)
                            self.enqueue('dead_queue', client_id,message['message'])
                    index = index + 1
                print(queues)
                return messages_to_send
        return False

    def push_messages(self, subscribers, topic_name, message_id, message):
        if(subscribers):
            for subscriber in subscribers:
                subscriber_id = subscriber[0]
                database_handler.insert_into_topic_queue(topic_name, subscriber_id, message_id, constants.MESSAGE_STATUS_READY)
                self.enqueue(topic_name, subscriber_id, message)
        return True

    def create_queue_for_topic(self, topic_name):
        database_handler.create_queue_for_topic(topic_name)
        if not queues.__contains__(topic_name):
            queues[topic_name] = []