import sys
sys.path.append('../../')
from DatabaseHandler.DatabaseHandler import DatabaseHandler
from Server.Services.Queue.Queue import Queue

database_handler = DatabaseHandler()
queue_obj = Queue()


class MessageHandler():

    def get_messages(self, topic_name, subscriber_id):
        # if we need from db
        # messages = database_handler.get_messages(topic_name, subscriber_id)
        # if we need from queue
        messages = queue_obj.get_messages(topic_name, subscriber_id)
        return messages

    def update_message_status(topic_name, subscriber_id):
        database_handler.update_message_status(topic_name, subscriber_id)
