import sys
sys.path.append('../../../')
import unittest
from Server.Services.MessageHandler.MessageHandler import MessageHandler

message_handler = MessageHandler()
class MessageHandlerTest(unittest.TestCase):
    def test_get_messages(self):
        topic_name = "ss"
        subscriber_id = 4
        self.assertTrue(type(message_handler.get_messages(topic_name, subscriber_id)))

if __name__ == '__main__':
    unittest.main()