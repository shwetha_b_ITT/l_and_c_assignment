import sys
sys.path.append('../')
from DatabaseHandler.DatabaseHandler import DatabaseHandler
database_handler = DatabaseHandler()

class ConnectionHandler:

    def connectToTopic(self):
        pass

    def disconnectFromTopic(self):
        pass

    def registerToTopic(self, client_port, topic_name):
        database_handler.insert_into_topic_table(topic_name, client_port)
        data = topic_name + client_port
        self.socket_handler.send_message(data, PUSH)
        response = self.socket_handler.get_response()
        return response

    def unregisterFromTopic(self, client_port, topic_name):
        database_handler.delete_from_table(topic_name, client_port)

    def checkTopic(self):
        pass