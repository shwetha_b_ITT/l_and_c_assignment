import json
import jsonpickle


class Parser:
    def serialize(self, data):
        return json.dumps(jsonpickle.encode(data, unpicklable=False), indent=4)

    def deserialize(self, data):
        data = json.loads(data)
        return json.loads(data)
