import sys
sys.path.append('../')
from Parser import Parser
import unittest

parser_obj = Parser()
class Test(unittest.TestCase):

    def test_serialize(self):
            string_data = "data"
            encoded_json = '"\\"data\\""'
            self.assertEqual(parser_obj.serialize(
                string_data), encoded_json)

    def test_serialize(self):
        employee = '{"id":"99"}'
        self.assertIsInstance(
            parser_obj.serialize(employee), str)

    def test_serialize(self):
            string_data = '"\\"data\\""'
            encoded_json = "data"
            self.assertEqual(parser_obj.deserialize(
                string_data), encoded_json)

if __name__ == '__main__':
    unittest.main()