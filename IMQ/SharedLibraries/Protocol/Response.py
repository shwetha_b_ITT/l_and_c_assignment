import sys
sys.path.append('../../')
from SharedLibraries.Protocol.Protocol import Protocol
from dataclasses import dataclass


@dataclass
class Response(Protocol):
    status: str = "successfully sent"
