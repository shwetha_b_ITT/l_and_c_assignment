from dataclasses import dataclass


@dataclass
class Protocol:
    data: str
    request_type: int
    format: str = "json"
