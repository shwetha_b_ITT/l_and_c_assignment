import sys
sys.path.append('../../')
from dataclasses import dataclass
from SharedLibraries.Protocol.Protocol import Protocol

@dataclass
class Request(Protocol):
    request_type: str = "client request"