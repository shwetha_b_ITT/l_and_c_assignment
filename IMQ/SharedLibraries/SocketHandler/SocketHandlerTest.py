import unittest
import sys
sys.path.append('../')
from SocketHandler import SocketHandler


class SocketHandlerTest(unittest.TestCase):
    def setUp(self):
        self.socket_handler = SocketHandler()

    def test_create_socket(self):
        self.assertTrue((type(self.socket_handler.create_socket())))

    def test_bind_socket(self):
        self.assertTrue(
            type(self.socket_handler.bind_socket()))

    def test_connect_socket(self):
        self.assertTrue(
            type(self.socket_handler.connect_socket()))

if __name__ == '__main__':
    unittest.main()
