import sys
sys.path.append('../../')
from Entities.Constants.Constants import Constants
from SharedLibraries.Parser.Parser import Parser
from SharedLibraries.Protocol.Response import Response
from SharedLibraries.Protocol.Request import Request
from ExceptionHandler.SocketException import SocketException
import json
import socket
from Entities.LogHandler.LogHandler import LogHandler

log_handler = LogHandler()
logger = log_handler.get_logger('imq_logger')
parser = Parser()
constants = Constants()


class SocketHandler:

    def __init__(self):
        self.socket_obj = socket.socket()
        self.port = constants.PORT
        self.host = socket.gethostname()

    def set_socket(self, socket):
        self.socket_obj = socket

    def create_socket(self):
        try:
            self.socket_obj = socket.socket()
            return True
        except SocketException:
            logger.error("socket creation failed")
            return False

    def bind_socket(self):
        try:
            logger.info("Binding the sockets : " + "Port: " +
                        str(self.port) + " Host: " + str(self.host) + " ")
            self.socket_obj.bind((self.host, self.port))
            self.socket_obj.listen(constants.MAX_CONNECTION)
        except SocketException:
            logger.error("socket binding failed" + "Host: " +
                         self.port + " Port: " + str(self.port) + " ")

    def connect_socket(self):
        try:
            print("waiting for connection..")
            self.socket_obj.connect((self.host, self.port))
            print("successfully connected..")
        except SocketException:
            logger.error("connection failed..")

    def communicate_with_server(self):
        while True:
            response_message = self.get_response()
            if response_message:
                logger.info('Received from server: ' + str(response_message))
                message = input(" Request from Client: -> ")
                try:
                    self.send_message(message, PUSH)
                except SocketException:
                    logger.error("Failed to receive")
        self.socket_obj.close()

    def send_message(self, message, type):
        try:
            request_data = Request(message, type)
            parsed_data = parser.serialize(request_data)
            print(parsed_data)
            self.socket_obj.send(str.encode(parsed_data))
        except SocketException:
            logger.error("Failed to send...\n")
            self.socket_obj.close()
            print('connection closed')

    def get_response(self):
        try:
            response_message = self.socket_obj.recv(1024)
            if not response_message:
                return False
            parsed_data = parser.deserialize(response_message)
            print(parsed_data)
            return parsed_data
        except SocketException:
            logger.error("Failed to receive..\n")
            self.socket_obj.close()
            print('connection closed')

    def accept_connection(self):
        try:
            connection, address = self.socket_obj.accept()
            return connection, address
        except SocketException:
            self.socket_obj.close()
            print('connection closed')

    def close_socket(self):
        try:
            self.socket_obj.close()
            print("Connection closed")
            return True
        except SocketException:
            self.socket_obj.close()
